<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedYorkshireHumbersideEnglandRegion extends Migration
{
    public $regionName = 'Yorkshire & Humberside';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $regionId = DB::table('regions')->insertGetId(
            [
                'name' => $this->regionName,
                'country_id' => 1,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        );

        DB::table('counties')->insert([
            [
                'name' => 'East Riding of Yorkshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'North Yorkshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'South Yorkshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'West Yorkshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('regions')->where('name', '=', $this->regionName)->delete();
    }

}
