<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedEastMidlandsRegion extends Migration
{
    public $regionName = 'East Midlands';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $regionId = DB::table('regions')->insertGetId(
            [
                'name' => $this->regionName,
                'country_id' => 1,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        );

        DB::table('counties')->insert([
            [
                'name' => 'Derbyshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Leicestershire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Lincolnshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Nottinghamshire',
                'region_id' => $regionId,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('regions')->where('name', '=', $this->regionName)->delete();
    }

}
