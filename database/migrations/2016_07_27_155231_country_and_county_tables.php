<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CountryAndCountyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Create country table
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 50);
            $table->timestamps();
        });

        // Add the first country
        DB::table('countries')->insert([
            [
                'name' => 'England',
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);

        // Add regions
        Schema::create('regions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->char('name', 50);
            $table->timestamps();

            $table->index('country_id');

            $table->foreign('country_id')
                  ->references('id')->on('countries')
                  ->onDelete('cascade');

        });

        // Add counties
        Schema::create('counties', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('region_id')->unsigned();
            $table->char('name', 50);
            $table->timestamps();

            $table->index('region_id');

            $table->foreign('region_id')
                  ->references('id')->on('regions')
                  ->onDelete('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counties');
        Schema::dropIfExists('regions');
        Schema::dropIfExists('countries');
    }
}
