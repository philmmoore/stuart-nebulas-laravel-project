var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    
	mix.less([
		'spark.less'
	], 'public/css/spark.css')

	mix.sass([
		'budgetbride.scss'
	], 'public/css/budgetbride.css')

	mix.styles([
		'public/css/spark.css',
		'public/css/budgetbride.css'
		], 'public/css/app.css',
		'./'
	)

   .browserify('app.js', null, null, { paths: 'vendor/laravel/spark/resources/assets/js' })
   .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
   .copy('node_modules/sweetalert/dist/sweetalert.css', 'public/css/sweetalert.css')
   .copy('resources/assets/img', 'public/img')
   .copy('resources/assets/fonts', 'public/fonts');

});
