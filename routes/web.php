<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Redirect Spark logged in user to their user account
Route::get('/home', function(){ 
	if (Auth::check()){
	    return Redirect::to('/supplier/'.Auth::user()->profile_slug, 301); 
	} else {
		return Redirect::to('/login/', 301); 
	}
});

// App hoome
Route::get('/', 'HomeController@show');

// Pretty up the registration url for supplier profiles creation
Route::get('/supplier/create-profile', '\Laravel\Spark\Http\Controllers\Auth\RegisterController@showRegistrationForm');
Route::post('/supplier/create-profile', '\Laravel\Spark\Http\Controllers\Auth\RegisterController@register');
Route::get('/supplier/{profile_slug}', 'Supplier\ProfileController@show');

// Spark Vue Endpoints

	// General
	Route::put('/settings/supplier/general', 'Spark\Settings\Supplier\GeneralController@update');

	// Banner
	Route::post('/settings/supplier/banner', 'Spark\Settings\Supplier\BannerController@update');

	// Categories
	Route::post('/settings/supplier/categories', 'Spark\Settings\Supplier\CategoriesController@update');
	Route::get('/settings/supplier/category/{id}', 'Spark\Settings\Supplier\CategoriesController@get');
	Route::get('/settings/supplier/categories/user', 'Spark\Settings\Supplier\CategoriesController@getUserCategories');

	// Media
	Route::post('/settings/supplier/media/upload', 'Spark\Settings\Supplier\MediaController@uploadFiles');
	Route::get('/settings/supplier/media', 'Spark\Settings\Supplier\MediaController@getMedia');
	Route::delete('/settings/supplier/media/{id}', 'Spark\Settings\Supplier\MediaController@deleteMedia');
