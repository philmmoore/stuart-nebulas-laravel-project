Vue.component('spark-supplier-profile', {
    props: ['user'],

    data() {
        return {
            map: {
                ready: false,
                position: ''
            }
        };
    },

    ready() {
    	var $this = this;

    	$(function(){

            if ($this.map.position){
        		var latLng = $this.map.position.split(',');
        		$this.setMap(parseFloat(latLng[0]), parseFloat(latLng[1]));
            }

            if ($('#media-gallery').length > 0){
                $("#media-gallery").unitegallery({
                    gallery_theme: "tiles",
                    tiles_type: "nested",
                    tiles_space_between_cols: 5,
                    lightbox_slider_control_zoom:false
                });
            }

    	});    	
    },

    methods: {
    	setMap(latitude, longitude) { 

            var $this = this;

            // create a JSON object with the values to mark the position
            var _position = { lat: latitude, lng: longitude};

            // add our default mapOptions
            var mapOptions = {
              zoom: 16,              // zoom level of the map
              center: _position     // position to center
            }

            // load a map within the "map" div and display
            var map = new google.maps.Map(document.getElementById('map'), mapOptions);

            // add a marker to the map with the position of the longitude and latitude
            var marker = new google.maps.Marker({
              position: mapOptions.center,
              map: map,
              draggable:true
            });

            // If the user moves the marker to be more accurate make sure we update the model
            marker.addListener('dragend', function(position){ 
                $this.setLatLng(position.latLng.lat(), position.latLng.lng());
            });

            $this.map.ready = true;

        }
    }

});
