var slug = require('slug');
slug.defaults.modes['pretty'] = {
    replacement: '-',
    symbols: true,
    lower: true,
    charmap: slug.charmap,
    multicharmap: slug.multicharmap
};

var base = require('auth/register-stripe');

Vue.component('spark-register-stripe', {
    mixins: [base],
    methods: {
    	formatSlug: function(value){
    		console.log('FORMAT SLUG');
    		this.registerForm.profile_slug = slug(value);
    	}
    },
    ready() {

		var $this = this;

		// Watches for changes on the company name to auto generate the users profile name
		$this.$watch('registerForm.company_name', function (val, oldVal) {
			$this.formatSlug(val);
		});

		// Watches for changes on the profile_slug incase the user decides to alter it manually
		$this.$watch('registerForm.profile_slug', function (val, oldVal) {
			$this.formatSlug(val);
		});

    }
});