var slug = require('slug');
slug.defaults.modes['pretty'] = {
    replacement: '-',
    symbols: true,
    lower: true,
    charmap: slug.charmap,
    multicharmap: slug.multicharmap
};

Vue.component('spark-supplier-general', {
    props: ['user'],

   /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                addressPostcode: '',
                locationLatLng: '',
                additionalLocation: []
            }),
            map: {
                ready: false
            }
        };
    },

    /**
     * Bootstrap the component.
     */
    ready() {
        
        var $this = this;

        // Watches for changes on the company name to auto generate the users profile name
        $this.$watch('form.company', function (val, oldVal) {
            $this.formatSlug(val);
        });

        // Watches for changes on the profile_slug incase the user decides to alter it manually
        $this.$watch('form.profile_slug', function (val, oldVal) {
            $this.formatSlug(val);
        });

        $(function(){

            // Init WYSIWYG description editor
            $('.summernote-description').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol']],
                ],
                placeholder: 'Write a little bit about your company here...',
                callbacks: {
                    onChange: function(contents, $editable) {
                        $this.form.description = contents;
                    }
                }
            });

            // Set starting map position if there's a postcode or latlng available
            $this.setStartingMapPosition();

        });
        
    },

    methods: {
        formatSlug: function(value){
            this.form.profile_slug = slug(value);
        },
        update() {
            var $this = this;
            Spark.put('/settings/supplier/general', $this.form)
                .then(function(response){
                    if (response.errors){
                        $this.hasErrors(response.errors);
                    }
                }).catch(function(response){
                    $this.hasErrors(response.errors);
                })
        },
        locate() {
            var $this = this;
            $this.getPosition($this.form.addressPostcode);
        },
        setStartingMapPosition() {
            var $this = this;

            if ($this.form.locationLatLng != ''){
                var position = $this.form.locationLatLng.split(',');
                var lat = parseFloat(position[0].trim());
                var lng = parseFloat(position[1].trim());
                $this.setMap(lat, lng);
            } else if ($this.form.addressPostcode != ''){
                $this.getPosition($this.form.addressPostcode);
                console.log('postcode');
            }

        },
        setLatLng(lat, lng) {
            var $this = this;
            $this.form.locationLatLng = lat + ', ' + lng;
        },
        getPosition(postcode) {

            var $this = this;

            // set up our geoCoder
            var geocoder = new google.maps.Geocoder();

            //send value to google to get a longitude and latitude value 
            geocoder.geocode({'address': postcode}, function(results, status) 
            {   
              // callback with a status and result
              if (status == google.maps.GeocoderStatus.OK){
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
                $this.setMap(lat, lng);
                $this.setLatLng(lat, lng);
              } else {
                $this.map.ready = false;
                $this.form.locationLatLng = '';
              }
            });
        },
        setMap(latitude, longitude) { 

            var $this = this;

            // create a JSON object with the values to mark the position
            var _position = { lat: latitude, lng: longitude};

            // add our default mapOptions
            var mapOptions = {
              zoom: 16,              // zoom level of the map
              center: _position     // position to center
            }

            // load a map within the "map" div and display
            var map = new google.maps.Map(document.getElementById('map'), mapOptions);

            // add a marker to the map with the position of the longitude and latitude
            var marker = new google.maps.Marker({
              position: mapOptions.center,
              map: map,
              draggable:true
            });

            // If the user moves the marker to be more accurate make sure we update the model
            marker.addListener('dragend', function(position){ 
                $this.setLatLng(position.latLng.lat(), position.latLng.lng());
            });

            $this.map.ready = true;

        },
        hasErrors: function(errors){
            var $this = this;
            $this.form.successful = false;
            $this.form.setErrors(errors);
        }
    },

    computed: {

    }

});
