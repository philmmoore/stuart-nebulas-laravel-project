Vue.component('spark-supplier-banner', {
    props: ['user'],

   /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({})
        };
    },

    /**
     * Bootstrap the component.
     */
    ready() {
        
    },

    methods: {
        update(e) {
            var $this = this;

            e.preventDefault();

            $this.form.startProcessing();

            // We need to gather a fresh FormData instance with the profile photo appended to
            // the data so we can POST it up to the server. This will allow us to do async
            // uploads of the profile photos. We will update the user after this action.
            $this.$http.post('/settings/supplier/banner', $this.gatherFormData())
                .then(function(response) {
                    $this.form.finishProcessing();
                    $('.bannerImage').css('background-image', 'url(' + response.data + ')');
                })
                .catch(function(response) {
                    $this.hasErrors(response.data.errors);
                });

        },
        /**
         * Gather the form data for the photo upload.
         */
        gatherFormData() {
            var $this = this;
            const data = new FormData();

            data.append('banner', $this.$els.banner.files[0]);
            return data;
        },
        hasErrors: function(errors){
            var $this = this;
            $this.form.successful = false;
            $this.form.setErrors(errors);
        }
    },

    computed: {

    }

});
