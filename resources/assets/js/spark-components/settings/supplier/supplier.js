Vue.component('spark-supplier', {
    props: ['user'],

   /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({})
        };
    },

    /**
     * Bootstrap the component.
     */
    ready() {
        
    },

    methods: {

    },

    computed: {

    }

});