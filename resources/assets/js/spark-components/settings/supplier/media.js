Vue.component('spark-supplier-media', {
    props: ['user'],

   /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                files: []
            }),
            token: '',
            csrf_token : '',
            dropzone: ''
        };
    },

    /**
     * Bootstrap the component.
     */
    ready() {
        
        var $this = this;

        Dropzone.autoDiscover = false;
        $this.dropzone = new Dropzone("div#dropzoneFileUpload", { 
            url: "/settings/supplier/media/upload",
            headers: {
                'X-CSRF-Token': $this.csrf_token
            },
            params: {
                _token: $this.token
            },
            dictDefaultMessage: 'Drop files here or click to select your files',
            addRemoveLinks: false,
            autoProcessQueue: false,
            parallelUploads: 50
        });
        
        $this.dropzone.on("addedfile", function(file) {
            // console.log(file);
        });

        $this.dropzone.on("removedfile", function(file) {
            // console.log(file);
        });

        $this.dropzone.on("error", function(file, response) {
            $this.hasErrors(response.errors);
        });

        $this.dropzone.on("sending", function() {
            // console.log('sending');
        });

        $this.dropzone.on("success", function(file, response) {
            $this.form.files.unshift(response);
            $this.dropzone.removeFile(file);
        });

        $this.dropzone.on("complete", function() {
            // $this.form.finishProcessing();
        });

        $this.dropzone.on("maxfilesexceeded", function(file){
            $this.dropzone.removeFile(file);
        });

        $this.getMedia();

    },

    methods: {
        upload: function(){
            var $this = this;
            $this.dropzone.processQueue();
        },
        deleteFile: function(index){
            var $this = this;
            if (confirm('Are you sure you want to remove this file?')){
                var file = $this.form.files[index];
                $this.$http.delete('/settings/supplier/media/' + file.id).then(function(response){
                    // console.log(response);
                    $this.form.files.splice(index, 1);
                }).catch(function(err){
                    console.log('There was an error removing the file');
                });
            }
        },
        getMedia: function(){
            var $this = this;
            $this.$http.get('/settings/supplier/media').then(function(response){
                $this.form.files = $.parseJSON(response.data);
            });
        },
        hasErrors: function(errors){
            var $this = this;
            $this.form.successful = false;
            $this.form.setErrors(errors);
        }
    },

    computed: {

    }

});
