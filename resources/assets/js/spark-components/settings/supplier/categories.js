Vue.component('spark-supplier-categories', {
    props: ['user'],

   /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
            	additional_category_ids: [],
            	additional_categories: []
            })
        };
    },

    /**
     * Bootstrap the component.
     */
    ready() {
       var $this = this;
       
       // Get the current users categories
       $this.getCategories();

       // Watch for changes on the additional_category_ids, this is where we'll handle adding and removing the fields
       // $this.$watch('form.additional_category_ids', function(newCategories, oldCategories){

       //      // Check to see if the array is of array type to push data
       //      if (!Array.isArray($this.form.additional_categories)){
       //          $this.form.additional_categories = [];
       //      }

       //      // Check to see if the category has been removed
       //      $.each(oldCategories, function(index, val){
       //          if (newCategories.indexOf(val) < 0){

       //              var category = val.split('|');
       //              var category_id = parseInt(category[0]);
       //              var category_name = category[1].trim();
       //              var removal_index;

       //              $.each($this.form.additional_categories, function(index, val){
       //                  if (val.category_id == category_id){
       //                      removal_index = index;
       //                  }
       //              });

       //              if (removal_index >= 0){
       //                  $this.form.additional_categories.splice(removal_index, 1);
       //              }

       //          }
       //      });

       //      // Check to see if it's a new category
       //      $.each(newCategories, function(index, val){
       //          if (oldCategories.indexOf(val) < 0){

       //              var category = val.split('|');
       //              var category_id = parseInt(category[0]);
       //              var category_name = category[1].trim();
                    
       //              // Loop through the current additional_categories, if the id doesn't 
       //              $this.form.additional_categories.push({
       //                  'name' : category_name,
       //                  'category_id' : category_id,
       //                  'prices_from' : '',
       //                  'prices_to' : '' 
       //              });

       //          }
       //      });

       // });

    },
    methods: {
        update() {
            var $this = this;
            Spark.post('/settings/supplier/categories', $this.form)
                .then(function(response){
                    if (response.errors){
                        $this.hasErrors(response.errors);
                    }
                }).catch(function(response){
                    $this.hasErrors(response.errors);
                });
        },
        getCategory(id) {
        	var $this = this;
	        return $this.$http.get('/settings/supplier/category/' + id).then(function(response){
	        	return Promise.resolve(response.data);
	        });
        },
        getCategories() {
        	var $this = this;
	        $this.$http.get('/settings/supplier/categories/user').then(function(response){
                $this.form.additional_categories = $.parseJSON(response.data)
            });
        },
        hasErrors: function(errors){
            var $this = this;
            $this.form.successful = false;
            $this.form.setErrors(errors);
        }
    },

    computed: {

    }

});