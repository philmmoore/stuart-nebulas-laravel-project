/**
 * Bootstrap for supplier
 */

require('./supplier/supplier');
require('./supplier/general');
require('./supplier/banner');
require('./supplier/categories');
require('./supplier/media');