@extends('spark::layouts.app')

@section('content')
	
<spark-supplier-profile :user="user" inline-template>

	<input type="hidden" name="position" v-model="map.position" value="{{ $profile->getMeta('locationLatLng') }}">

	<div class="tile supplier-header" style="background-image: url({{ $profile->getMeta('banner') }});">
		<div class="container">
			<div class="details">
				<div class="avatar">
					<div class="image" style="background-image: url({{ $profile->photo_url }});"></div>
				</div>
				<div class="content">
					<div class="name">{{ $profile->company_name }}</div>
					<div class="joined">Joined {{ $profile->created_at->format('F Y') }}</div>
					<div class="actions">
						<ul>
							{{-- <li>
								<div class="object button-style-two favourite">
									<button>
										<i class="fa fa-btn fa-star"></i>
										<span>Add to Favourites</span>
									</button>
								</div>
							</li> --}}
							<li>
								<div class="object button-style-two supplier">
									<button>
										<i class="fa fa-btn fa-commenting"></i>
										<span>Ask a Question</span>
									</button>
								</div>
							</li>
						</ul>
					</div>
					<div class="prices">
						<span class="price-label">Prices From</span>
						<span class="price">£{{ $profile->getPrimaryCategory()->prices_from }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="overflow-hidden">
		<div class="tile profile-section gallery">
			<div class="object gallery" id="media-gallery">
				@foreach ($profile->media()->get() as $media)
					<img 
					data-image="{{ $media->resource }}"
					src="{{ $media->thumbnail }}" alt="" />
				@endforeach
			</div>
		</div>
	</div>

	<div class="tile profile-section">
		<div class="container">
			<div class="content">
				<h2 class="heading">About {{ $profile->company_name }}</h2>
				{!! $profile->getMeta('description') !!}
			</div><div class="sidebar">
				<div class="contact">
					<ul>
						@if ($phone = $profile->getMeta('telephone'))
							<li>
								<i class="fa fa-btn fa-phone-square"></i>
								<span>{{ $phone }}</span>
							</li>
						@endif
						@if ($mobile = $profile->getMeta('mobile'))
							<li>
								<i class="fa fa-btn fa-mobile-phone"></i>
								<span>{{ $mobile }}</span>
							</li>
						@endif
						@if ($fax = $profile->getMeta('fax'))
							<li>
								<i class="fa fa-btn fa-fax"></i>
								<span>{{ $fax }}</span>
							</li>
						@endif
						@if ($website = $profile->getMeta('website'))
							<li>
								<i class="fa fa-btn fa-link"></i>
								<span><a href="{{ $website }}" target="_blank">Visit Website</a></span>
							</li>
						@endif
						@if ($facebook = $profile->getMeta('social_facebook'))
							<li>
								<i class="fa fa-btn fa-facebook-square"></i>
								<span><a href="{{ $facebook }}" target="_blank">{{ $facebook }}</a></span>
							</li>
						@endif
						@if ($twitter = $profile->getMeta('social_twitter'))
							<li>
								<i class="fa fa-btn fa-twitter-square"></i>
								<span><a href="{{ $twitter }}" target="_blank">{{ $twitter }}</a></span>
							</li>
						@endif
						@if ($instagram = $profile->getMeta('social_instagram'))
							<li>
								<i class="fa fa-btn fa-instagram"></i>
								<span><a href="{{ $instagram }}" target="_blank">{{ $instagram }}</a></span>
							</li>
						@endif
						@if ($address = $profile->getFullAddress())
							<li>
								<i class="fa fa-btn fa-map-marker"></i>
								<span>
									@foreach ($address as $address_value)
									    {{ $address_value }}<br />
									@endforeach
								</span>
							</li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="tile profile-section" v-if="map.position">
		<div class="container">
			<h2 class="heading">Location</h2>
			<div class="object map">
				<div id="map"></div>
			</div>
		</div>
	</div>

</spark-supplier-profile>

@endsection