@extends('spark::layouts.app')

@section('content')
	
<spark-home :user="user" inline-template>

	<div class="tile full-height-container secondary-overlay">
		<div class="container">

			<div class="object vertical-align">
				<div class="wrapper">
					{{-- <h1 class="heading">You found the ❤️ of your life so let us help piece together your special day.</h1> --}}
					<form method="post" class="form margin-top">
						<ul>
							<li>
								<div class="object dropdown">
									<label>I'm looking for</label>
									<select class="selectpicker" name="category" data-hide-disabled="true" data-size="6" data-dropup-auto="false" data-live-search="true">
										@foreach(App\Models\Categories\Category::all() as $category)
										    <option 
										    	@if ($category->id == 33)
											    	selected="selected"
										    	@endif
										        data-tokens="{{ $category->name }}" 
										        value="{{ $category->id }}">{{ $category->name }}</option>
										@endforeach
									</select>
								</div>
							</li><li>
								<div class="object dropdown">
									<label>Location</label>
			                        <select class="selectpicker" name="addressCounty" data-size="6" data-dropup-auto="false" data-live-search="true">
			                            
			                            @foreach(App\Models\Location\Country::find(1)->regions()->get() as $region)
			                                
			                                <optgroup label="{{ $region->name }}">
			                                    @foreach($region->counties()->get() as $county)
			                                        <option 
			                                        data-tokens="{{ $county->name }}" value="{{ $county->id }}">{{ $county->name }}</option>
			                                    @endforeach
			                                </optgroup>

			                            @endforeach

			                        </select>
								</div>
							</li><li>
								<div class="object dropdown pink">
									<label>Looking to spend</label>
									<select class="selectpicker" name="price" data-size="6" data-dropup-auto="false" data-live-search="true">
										<option>£3500</option>
										<option>£3000</option>
										<option>£2000</option>
									</select>
								</div>
							</li>
						</ul>
						<div class="search">
							<div class="object button-style-one has-right-arrow">
								<button type="submit">Find Suppliers</button>
							</div>
						</div>
						<div class="register">
							<a href="/supplier/create-profile/">Create your business profile</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</spark-home>

@endsection