<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', 'BudgetBrideUK Ltd')</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

    <script src="{{ asset('vendor/dropzone/dropzone.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/dropzone.css') }}"> 

    <link rel="stylesheet" href="{{ asset('vendor/unitegallery/css/unite-gallery.css') }}"> 

    <!-- Scripts -->
    @yield('scripts', '')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>
</head>
<body v-cloak>

    <div id="spark-app">

        <!-- Header -->
        <div class="tile header">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <picture>
                           <source srcset="/img/budget-bride-logo@2x.png 2x, /img/budget-bride-logo.png 1x" />
                           <img src="/img/budget-bride.png" alt="BudgetBrideUK Logo" />
                        </picture>
                    </a>
                </div>
                <nav class="nav">
                    <ul>
                        @if (!Auth::check())
                            <li class="outline">
                                <a href="/supplier/create-profile/">
                                    <i class="fa fa-btn fa-briefcase"></i>
                                    <span>Create your Business Profile</span>
                                </a>
                            </li>
                            <li class="or">or</li>
                            <li class="fill">
                                <a href="/login/">
                                <i class="fa fa-btn fa-sign-in"></i>
                                    <span>Login</span>
                                </a>
                            </li>
                        @else
                            @if (!isset($isLoggedInUser) || !$isLoggedInUser )
                                <li class="outline">
                                    <a href="/supplier/@{{ user.profile_slug }}">
                                        <i class="fa fa-btn fa-briefcase"></i>
                                        <span>@{{ user.company_name }}</span>
                                    </a>
                                </li>
                            @endif
                            <li class="fill">
                                <a href="/settings/">
                                    <i class="fa fa-btn fa-cog"></i>
                                    <span>Account Settings</span>
                                </a>
                            </li>
                            {{-- <li class="or">or</li>
                            <li class="fill red">
                                <a href="/logout/">
                                    <i class="fa fa-btn fa-sign-out"></i>
                                    <span>Logout</span>
                                </a>
                            </li> --}}
                        @endif
                    </ul>
                </nav>
            </div>
        </div>

        <!-- Main Content -->
        @yield('content')

        <!-- Footer -->
        <div class="tile footer">
            <div class="container">
                <div class="block content">
                    <div class="copyright">&copy; BudgetBrideUK Ltd. 2016</div>
                    <div class="company">Company no. 10280504</div>
                </div><div class="block social">
                    <ul>
                        <li>
                            <a class="facebook" href="https://www.facebook.com/BudgetBrideUK/" target="_blank">
                                <img srcset="/img/footer-facebook@2x.png 2x, /img/footer-facebook.png 1x" src="/img/footer-facebook.png" alt="Find BudgetBrideUK on Facebook" />
                            </a>
                        </li><li>
                            <a class="twitter" href="https://twitter.com/budgetbrideuk" target="_blank">
                                <img srcset="/img/footer-twitter@2x.png 2x, /img/footer-twitter.png 1x" src="/img/footer-twitter.png" alt="Follow BudgetBrideUK on Twitter" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Application Level Modals -->
        @if (Auth::check())
            @include('spark::modals.notifications')
            @include('spark::modals.support')
            @include('spark::modals.session-expired')
        @endif

        <!-- Google Maps API -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZo9u0w-qeb-EJtU-4rf_Zk4o1ltRGCHU&libraries=places"></script> 

        <!-- JavaScript -->
        <script src="/js/sweetalert.min.js"></script>
        <script src="/js/app.js"></script>

        <!-- Bootstrap Select -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

        <!-- include summernote css/js-->
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

        <script src="{{ asset('vendor/unitegallery/js/unitegallery.min.js') }}"></script>
        <script src="{{ asset('vendor/unitegallery/themes/tiles/ug-theme-tiles.js') }}"></script>

    </div>
</body>
</html>
