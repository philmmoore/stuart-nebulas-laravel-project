<spark-supplier :user="user" inline-template>
	<div class="panel panel-default">
		<div class="panel-body">

			<!-- Tab panes -->
			<div class="tab-content">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#company-details" aria-controls="company-details" role="tab" data-toggle="tab">Company Details</a></li>
					<li role="presentation"><a href="#products-services" aria-controls="products-services" role="tab" data-toggle="tab">Products &amp; Services</a></li>
					<li role="presentation"><a href="#images-videos" aria-controls="images-videos" role="tab" data-toggle="tab">Images &amp; Videos</a></li>
					<li role="presentation"><a href="#your-details" aria-controls="your-details" role="tab" data-toggle="tab">Your Details</a></li>
				</ul>

				<div class="clearfix">&nbsp;</div>

				<div role="tabpanel" class="tab-pane active" id="company-details">

			        <!-- Update Profile Photo -->
			        @include('spark::settings.profile.update-profile-photo')

			        <!-- Update Banner -->
			        @include('spark::settings.supplier.update-banner')

			        <!-- Update General Information -->
			        @include('spark::settings.supplier.update-general-inforamation')

				</div>
				<div role="tabpanel" class="tab-pane" id="products-services">

			        <!-- Categories -->
			        @include('spark::settings.supplier.update-categories')

				</div>
				<div role="tabpanel" class="tab-pane" id="images-videos">

			        <!-- Update Media -->
			        @include('spark::settings.supplier.update-media')

				</div>

				<div role="tabpanel" class="tab-pane" id="your-details">

			        <!-- Update Contact Information -->
			        @include('spark::settings.profile.update-contact-information')

				    <!-- Update Password -->
				    @include('spark::settings.security.update-password')

				    <!-- Two-Factor Authentication -->
				    @if (Spark::usesTwoFactorAuth())
				    	<div v-if="user && ! user.uses_two_factor_auth">
				    		@include('spark::settings.security.enable-two-factor-auth')
				    	</div>

				    	<div v-if="user && user.uses_two_factor_auth">
				    		@include('spark::settings.security.disable-two-factor-auth')
				    	</div>

						<!-- Two-Factor Reset Code Modal -->
				    	@include('spark::settings.security.modals.two-factor-reset-code')
				    @endif

				</div>

			</div>
		</div>
	</div>
</spark-supplier>
