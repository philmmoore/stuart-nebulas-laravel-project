<spark-supplier-categories :user="user" inline-template>

    <div class="panel panel-default">
        <div class="panel-heading">Business Categories</div>
        <div class="panel-body">

            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your information has been updated!
            </div>

            <!-- Internal Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('internal')">
                @{{ form.errors.get('internal') }}
            </div>

            <!-- Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('banner')">
                @{{ form.errors.get('banner') }}
            </div>

            <form class="form-horizontal" role="form">

                <!-- Primary Category -->
                <div class="form-group" :class="{'has-error': form.errors.has('category')}">
                    <label class="col-md-4 control-label">Primary Category</label>
                    <div class="col-md-6">
                        <select class="selectpicker" name="category" v-model="form.category"  data-live-search="true">
                            @foreach(App\Models\Categories\Category::all() as $category)
                                <option 
                                    @if ( Auth::user()->getPrimaryCategory()->category_id == $category->id ) 
                                        selected="selected"
                                    @endif 
                                    data-tokens="{{ $category->name }}" 
                                    value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <span class="help-block" v-show="form.errors.has('category')">
                            @{{ form.errors.get('category') }}
                        </span>
                    </div>
                </div>

                <!-- Prices From -->
                <div class="form-group" :class="{'has-error': form.errors.has('category_from')}" v-show="form.category">
                    <label class="col-md-4 control-label">Prices From</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="category_from" v-model="form.category_from" value="{{ Auth::user()->getPrimaryCategory()->prices_from }}">
                        <span class="help-block" v-show="form.errors.has('category_from')">
                            @{{ form.errors.get('category_from') }}
                        </span>
                    </div>
                </div>

                <!-- Prices To -->
                <div class="form-group" :class="{'has-error': form.errors.has('category_to')}" v-show="form.category_from">
                    <label class="col-md-4 control-label">Prices Upto</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="category_to" v-model="form.category_to" value="{{ Auth::user()->getPrimaryCategory()->prices_to }}">
                        <span class="help-block" v-show="form.errors.has('category_to')">
                            @{{ form.errors.get('category_to') }}
                        </span>
                    </div>
                </div>

                <hr />

                <!-- Additional Category -->
                <div class="form-group" :class="{'has-error': form.errors.has('additional_category_ids')}">
                    <label class="col-md-4 control-label">Additional Categories</label>
                    <div class="col-md-6">
                        <select class="selectpicker" name="category" v-model="form.additional_category_ids" data-live-search="true" multiple data-max-options="3">
                            @foreach(App\Models\Categories\Category::all() as $category)
                                @if ( Auth::user()->getPrimaryCategory()->category_id != $category->id ) 
                                    <option 
                                    @if ( Auth::user()->hasCategory($category->id) ) 
                                        selected="selected"
                                    @endif 
                                        data-tokens="{{ $category->name }}" 
                                        value="{{ $category->id }}|{{ $category->name }}">{{ $category->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="help-block" v-show="form.errors.has('additional_category_ids')">
                            @{{ form.errors.get('additional_category_ids') }}
                        </span>
                    </div>
                </div>

                <div class="form-group" v-for="category in form.additional_categories" :key="category.category_id" track-by="$index">
                    <label class="col-md-4 control-label">@{{ category.name }}</label>
                    <div class="col-md-3" :class="{'has-error': form.errors.has('additional_categories.' + $index + '.prices_from')}">
                        <input type="text" class="form-control" v-model="form.additional_categories[$index]['prices_from']" value="@{{category.prices_from}}">
                        <span class="help-block" v-show="form.errors.has('additional_categories.' + $index + '.prices_from')">
                            @{{ form.errors.get('additional_categories.' + $index + '.prices_from') }}
                        </span>
                    </div>
                    <div class="col-md-3" :class="{'has-error': form.errors.has('additional_categories.' + $index + '.prices_to')}">
                        <input type="text" class="form-control" v-model="form.additional_categories[$index]['prices_to']" value="@{{category.prices_to}}">
                        <span class="help-block" v-show="form.errors.has('additional_categories.' + $index + '.prices_to')">
                            @{{ form.errors.get('additional_categories.' + $index + '.prices_to') }}
                        </span>
                    </div>
                </div>

                <hr />

                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" @click.prevent="update" :disabled="form.busy">
                            Update
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>



</spark-supplier-categories>