<spark-supplier-banner :user="user" inline-template>

    <div class="panel panel-default">
        <div class="panel-heading">Banner Image</div>
        <div class="panel-body">

            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your banner image has been updated!
            </div>

            <!-- Internal Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('internal')">
                @{{ form.errors.get('internal') }}
            </div>

            <!-- Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('banner')">
                @{{ form.errors.get('banner') }}
            </div>

            <form class="form-horizontal" role="form">

                <!-- Photo Preview-->
                <div class="form-group">
                    <div class="col-md-12">
                        <span role="img" class="bannerImage" style="background-image: url({{ Auth::user()->getMeta('banner') }}); height: 300px; width: 100%; background-size:cover; display:block;">
                        </span>
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label">&nbsp;</label>

                    <div class="col-md-6">
                        <label type="button" class="btn btn-primary btn-upload" :disabled="form.busy">
                            <span>Select New Photo</span>
                            <input v-el:banner type="file" class="form-control" name="banner" @change="update">
                        </label>
                    </div>
                </div>

            </form>

        </div>
    </div>

</spark-supplier-banner>