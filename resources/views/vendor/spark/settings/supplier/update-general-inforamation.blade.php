<spark-supplier-general :user="user" inline-template>

    <div class="panel panel-default">
        <div class="panel-heading">General Information</div>
        <div class="panel-body">

            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your information has been updated!
            </div>

            <!-- Internal Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('internal')">
                @{{ form.errors.get('internal') }}
            </div>

            <!-- Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('banner')">
                @{{ form.errors.get('banner') }}
            </div>

            <form class="form-horizontal" role="form">

                <!-- Company Name -->
                <div class="form-group" :class="{'has-error': form.errors.has('company')}">
                    <label class="col-md-4 control-label">Company Name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="company" v-model="form.company" value="{{ Auth::user()->company_name }}">
                        <span class="help-block" v-show="form.errors.has('company')">
                            @{{ form.errors.get('company') }}
                        </span>
                    </div>
                </div>

                <div class="form-group" :class="{'has-error': form.errors.has('profile_slug')}">
                    <label class="col-md-4 control-label">Profile URL</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="profile_slug" v-model="form.profile_slug" value="{{ Auth::user()->profile_slug }}">
                        <span class="help-block" v-show="form.errors.has('profile_slug')">
                            @{{ form.errors.get('profile_slug') }}
                        </span>
                    </div>
                </div>

                <!-- Address -->
                <div class="form-group" :class="{'has-error': form.errors.has('addressLine1')}">
                    <label class="col-md-4 control-label">Address</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="addressLine1" v-model="form.addressLine1" value="{{ Auth::user()->getMeta('addressLine1') }}" placeholder="Address Line 1">
                        <span class="help-block" v-show="form.errors.has('addressLine1')">
                            @{{ form.errors.get('addressLine1') }}
                        </span>
                    </div>
                </div>

                <div class="form-group" :class="{'has-error': form.errors.has('addressLine2')}">
                    <div class="col-md-6 col-md-offset-4">
                        <input type="text" class="form-control" name="addressLine2" v-model="form.addressLine2" value="{{ Auth::user()->getMeta('addressLine2') }}" placeholder="Address Line 2">
                        <span class="help-block" v-show="form.errors.has('addressLine2')">
                            @{{ form.errors.get('addressLine2') }}
                        </span>
                    </div>
                </div>

                <div class="form-group" :class="{'has-error': form.errors.has('addressTownCity')}">
                    <div class="col-md-6 col-md-offset-4">
                        <input type="text" class="form-control" name="addressTownCity" v-model="form.addressTownCity" value="{{ Auth::user()->getMeta('addressTownCity') }}" placeholder="Town/City">
                        <span class="help-block" v-show="form.errors.has('addressTownCity')">
                            @{{ form.errors.get('addressTownCity') }}
                        </span>
                    </div>
                </div>

                <div class="form-group" :class="{'has-error': form.errors.has('addressCounty')}">
                    <div class="col-md-6 col-md-offset-4">
                        
                        <!-- <input type="text" class="form-control" name="addressCounty" v-model="form.addressCounty" value="{{ Auth::user()->getMeta('addressCounty') }}" placeholder="County"> -->

                        <select class="selectpicker" name="addressCounty" v-model="form.addressCounty"  data-live-search="true">
                            
                            @foreach(App\Models\Location\Country::find(1)->regions()->get() as $region)
                                
                                <optgroup label="{{ $region->name }}">
                                    @foreach($region->counties()->get() as $county)
                                        <option 
                                            @if ( Auth::user()->getMeta('addressCounty') == $county->id ) 
                                                selected="selected"
                                            @endif 
                                        data-tokens="{{ $county->name }}" value="{{ $county->id }}">{{ $county->name }}</option>
                                    @endforeach
                                </optgroup>

                            @endforeach

                        </select>

                        <span class="help-block" v-show="form.errors.has('addressCounty')">
                            @{{ form.errors.get('addressCounty') }}
                        </span>
                    </div>
                </div>

                <div class="form-group" :class="{'has-error': form.errors.has('addressPostcode')}">

                    <div class="col-md-3 col-md-offset-4">
                        <input type="text" class="form-control" name="addressPostcode" v-model="form.addressPostcode" value="{{ Auth::user()->getMeta('addressPostcode') }}" placeholder="Postcode">
                        <span class="help-block" v-show="form.errors.has('addressPostcode')">
                            @{{ form.errors.get('addressPostcode') }}
                        </span>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-block btn-primary" @click.prevent="locate" :disabled="form.busy">
                            Locate
                        </button>
                    </div>
                </div>

                <div class="form-group" v-show="map.ready">
                    <div class="col-md-6 col-md-offset-4">
                        <div id="map" style="display: block; width:100%; height: 300px;"></div>
                        <input type="hidden" class="form-control" name="locationLatLng" v-model="form.locationLatLng" value="{{ Auth::user()->getMeta('locationLatLng') }}" />
                    </div>
                </div>

                <hr />

                <div class="form-group" :class="{'has-error': form.errors.has('additionalLocations')}">
                    <label class="col-md-4 control-label">Additional Locations</label>
                    <div class="col-md-6">                        

                        <select class="selectpicker" name="additionalLocations" v-model="form.additionalLocations" data-live-search="true" multiple multiple data-max-options="3">
                            
                            @foreach(App\Models\Location\Country::find(1)->regions()->get() as $region)
                                
                                <optgroup label="{{ $region->name }}">
                                    @foreach($region->counties()->get() as $county)
                                        <option  
                                            @if ( Auth::user()->hasLocation($county->id, true) ) 
                                                selected="selected"
                                            @endif 
                                        data-tokens="{{ $county->name }}" value="{{ $county->id }}">{{ $county->name }}</option>
                                    @endforeach
                                </optgroup>

                            @endforeach

                        </select>

                        <span class="help-block" v-show="form.errors.has('additionalLocations')">
                            @{{ form.errors.get('additionalLocations') }}
                        </span>
                    </div>
                </div>

                <hr />

                <!-- Telephone -->
                <div class="form-group" :class="{'has-error': form.errors.has('telephone')}">
                    <label class="col-md-4 control-label">Telephone</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="telephone" v-model="form.telephone" value="{{ Auth::user()->getMeta('telephone') }}">
                        <span class="help-block" v-show="form.errors.has('telephone')">
                            @{{ form.errors.get('telephone') }}
                        </span>
                    </div>
                </div>

                <!-- Fax -->
                <div class="form-group" :class="{'has-error': form.errors.has('fax')}">
                    <label class="col-md-4 control-label">Fax</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="fax" v-model="form.fax" value="{{ Auth::user()->getMeta('fax') }}">
                        <span class="help-block" v-show="form.errors.has('fax')">
                            @{{ form.errors.get('fax') }}
                        </span>
                    </div>
                </div>

                <!-- Mobile -->
                <div class="form-group" :class="{'has-error': form.errors.has('mobile')}">
                    <label class="col-md-4 control-label">Mobile</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="mobile" v-model="form.mobile" value="{{ Auth::user()->getMeta('mobile') }}">
                        <span class="help-block" v-show="form.errors.has('mobile')">
                            @{{ form.errors.get('mobile') }}
                        </span>
                    </div>
                </div>

                <hr />

                <!-- Website -->
                <div class="form-group" :class="{'has-error': form.errors.has('website')}">

                    <label class="col-md-4 control-label">Website</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="website" v-model="form.website" value="{{ Auth::user()->getMeta('website') }}">

                        <span class="help-block" v-show="form.errors.has('website')">
                            @{{ form.errors.get('website') }}
                        </span>
                    </div>

                </div>

                <!-- Facebook -->
                <div class="form-group" :class="{'has-error': form.errors.has('social_facebook')}">

                    <label class="col-md-4 control-label">Facebook</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="social_facebook" v-model="form.social_facebook" value="{{ Auth::user()->getMeta('social_facebook') }}">

                        <span class="help-block" v-show="form.errors.has('social_facebook')">
                            @{{ form.errors.get('social_facebook') }}
                        </span>
                    </div>

                </div>

                <!-- Twitter -->
                <div class="form-group" :class="{'has-error': form.errors.has('social_twitter')}">

                    <label class="col-md-4 control-label">Twitter</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="social_twitter" v-model="form.social_twitter" value="{{ Auth::user()->getMeta('social_twitter') }}">

                        <span class="help-block" v-show="form.errors.has('social_twitter')">
                            @{{ form.errors.get('social_twitter') }}
                        </span>
                    </div>

                </div>

                <!-- Instagram -->
                <div class="form-group" :class="{'has-error': form.errors.has('social_instagram')}">

                    <label class="col-md-4 control-label">Instagram</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="social_instagram" v-model="form.social_instagram" value="{{ Auth::user()->getMeta('social_instagram') }}">

                        <span class="help-block" v-show="form.errors.has('social_instagram')">
                            @{{ form.errors.get('social_instagram') }}
                        </span>
                    </div>

                </div>

                <hr />

                <!-- Description -->
                <div class="form-group" :class="{'has-error': form.errors.has('description')}">

                    <label class="col-md-4 control-label">Description</label>
                    <div class="col-md-6">
                        <textarea class="summernote-description form-control" name="description" v-model="form.description">{{ Auth::user()->getMeta('description') }}</textarea>
                        <span class="help-block" v-show="form.errors.has('description')">
                            @{{ form.errors.get('description') }}
                        </span>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" @click.prevent="update" :disabled="form.busy">
                            Update
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

</spark-supplier-general>