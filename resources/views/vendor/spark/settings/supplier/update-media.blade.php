<spark-supplier-media :user="user" inline-template>

    <div class="panel panel-default">
        <div class="panel-heading">Media</div>
        <div class="panel-body">

            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your information has been updated!
            </div>

            <!-- Internal Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('internal')">
                @{{ form.errors.get('internal') }}
            </div>

            <!-- Error Message -->
            <div class="alert alert-danger" v-if="form.errors.has('file')">
                @{{ form.errors.get('file') }}
            </div>

            <form class="form-horizontal" role="form">

                <input type="hidden" name="csrf_token" v-model="csrf_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="token" v-model="token" value="{{ Session::getToken() }}" />
                <div class="dropzone-container">
                    <div class="dropzone" id="dropzoneFileUpload">

                    </div>
                </div>

                <br />
  
                <div class="form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-block" @click.prevent="upload" :disabled="form.busy">
                            Upload Files
                        </button>
                    </div>
                </div>

                <hr />

                <ul class="files">
                    <li v-for="file in form.files" track-by="$index">
                        <span class="name">@{{file.resource}}</span>
                        <br />
                        <br />
                        <button class="btn btn-primary btn-danger" @click.prevent="deleteFile($index)">
                            Remove
                        </button>
                        <hr />
                    </li>
                </ul>

            </form>

        </div>
    </div>

</spark-supplier-media>