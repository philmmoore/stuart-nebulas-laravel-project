@extends('spark::layouts.app')

@section('content')

<div class="tile full-height-container auth secondary-overlay">
	<div class="container">
		<div class="object vertical-align">
			<div class="wrapper">

                <div class="object inner-container">

                    <div class="object intro">
                    	<i class="fa fa-lock" aria-hidden="true"></i>
	                    <h1 class="object heading-style-one">Login to your account</h1>
                    </div>

                    <form role="form" method="POST" action="/login">
                        {{ csrf_field() }}

                        @include('spark::shared.errors')

                        <input type="hidden" name="remember" />

                        <ul>
	                        <li>
		                        <!-- E-Mail Address -->
		                        <div class="object input">
		                            <label for="email" class="control-label">E-Mail Address</label>
		                            <div class="input-wrapper">
		                                <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="john.smith@budgetbride.co.uk" autofocus>
		                            </div>
		                        </div>
	                        </li>
	                        <li>
		                        <!-- Password -->
		                        <div class="object input">
		                            <label class="control-label" for="password">Password</label>
		                            <div class="input-wrapper">
		                                <input type="password" id="password" name="password" placeholder="••••••">
		                            </div>
		                        </div>
                        	</li>
                        	{{-- <li>
		                        <!-- Remember Me -->
		                        <div class="object checkbox">
		                            <div class="checkbox">
		                                <label>
		                                    <input type="checkbox" name="remember"> Remember Me
		                                </label>
		                            </div>
		                        </div>
                        	</li> --}}
                        	<li class="submit">
                                <div class="object button-style-two black-primary">
	                                <button type="submit">Login</button>
                                </div>
	                        </li>
                            <li class="create">
                                <a class="object link-style" href="/register/">Create your free account</a>
                            </li>
                            <li class="forgotten">
                                <a href="{{ url('/password/reset') }}">Help! I've forgotten my password!</a>
                            </li>
                        </ul>

                    </form>
                </div>

			</div>
		</div>
	</div>
</div>

{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    @include('spark::shared.errors')

                    <form class="form-horizontal" role="form" method="POST" action="/login">
                        {{ csrf_field() }}

                        <!-- E-Mail Address -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <!-- Remember Me -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <!-- Login Button -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa m-r-xs fa-sign-in"></i>Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 --}}

@endsection
