{{-- <!-- Coupon -->
<div class="row" v-if="coupon">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-success">
            <div class="panel-heading">Discount</div>

            <div class="panel-body">
                The coupon's @{{ discount }} discount will be applied to your subscription!
            </div>
        </div>
    </div>
</div>

<!-- Invalid Coupon -->
<div class="row" v-if="invalidCoupon">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-danger">
            Whoops! This coupon code is invalid.
        </div>
    </div>
</div>

<!-- Invitation -->
<div class="row" v-if="invitation">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-success">
            We found your invitation to the <strong>@{{ invitation.team.name }}</strong> team!
        </div>
    </div>
</div>

<!-- Invalid Invitation -->
<div class="row" v-if="invalidInvitation">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-danger">
            Whoops! This invitation code is invalid.
        </div>
    </div>
</div>

<!-- Plan Selection -->
<div class="row" v-if="paidPlans.length > 0">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-left" :class="{'btn-table-align': hasMonthlyAndYearlyPlans}">
                    Subscription
                </div>

                <!-- Interval Selector Button Group -->
                <div class="pull-right">
                    <div class="btn-group" v-if="hasMonthlyAndYearlyPlans" style="padding-top: 2px;">
                        <!-- Monthly Plans -->
                        <button type="button" class="btn btn-default"
                                @click="showMonthlyPlans"
                                :class="{'active': showingMonthlyPlans}">

                            Monthly
                        </button>

                        <!-- Yearly Plans -->
                        <button type="button" class="btn btn-default"
                                @click="showYearlyPlans"
                                :class="{'active': showingYearlyPlans}">

                            Yearly
                        </button>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="panel-body spark-row-list">
                <!-- Plan Error Message - In General Will Never Be Shown -->
                <div class="alert alert-danger" v-if="registerForm.errors.has('plan')">
                    @{{ registerForm.errors.get('plan') }}
                </div>

                <!-- European VAT Notice -->
                @if (Spark::collectsEuropeanVat())
                    <p class="p-b-md">
                        All subscription plan prices are excluding applicable VAT.
                    </p>
                @endif

                <table class="table table-borderless m-b-none">
                    <thead></thead>
                    <tbody>
                        <tr v-for="plan in plansForActiveInterval">
                            <!-- Plan Name -->
                            <td>
                                <div class="btn-table-align" @click="showPlanDetails(plan)">
                                    <span style="cursor: pointer;">
                                        <strong>@{{ plan.name }}</strong>
                                    </span>
                                </div>
                            </td>

                            <!-- Plan Features Button -->
                            <td>
                                <button class="btn btn-default m-l-sm" @click="showPlanDetails(plan)">
                                    <i class="fa fa-btn fa-star-o"></i>Plan Features
                                </button>
                            </td>

                            <!-- Plan Price -->
                            <td>
                                <div class="btn-table-align">
                                    <span v-if="plan.price == 0">
                                        Free
                                    </span>

                                    <span v-else>
                                        @{{ plan.price | currency spark.currencySymbol }} / @{{ plan.interval | capitalize }}
                                    </span>
                                </div>
                            </td>

                            <!-- Trial Days -->
                            <td>
                                <div class="btn-table-align" v-if="plan.trialDays">
                                    @{{ plan.trialDays}} Day Trial
                                </div>
                            </td>

                            <!-- Plan Select Button -->
                            <td class="text-right">
                                <button class="btn btn-primary btn-plan" v-if="isSelected(plan)" disabled>
                                    <i class="fa fa-btn fa-check"></i>Selected
                                </button>

                                <button class="btn btn-primary-outline btn-plan" @click="selectPlan(plan)" v-else>
                                    Select
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Basic Profile -->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span v-if="paidPlans.length > 0">
                    Profile
                </span>

                <span v-else>
                    Register
                </span>
            </div>

            <div class="panel-body">
                <!-- Generic Error Message -->
                <div class="alert alert-danger" v-if="registerForm.errors.has('form')">
                    @{{ registerForm.errors.get('form') }}
                </div>

                <!-- Invitation Code Error -->
                <div class="alert alert-danger" v-if="registerForm.errors.has('invitation')">
                    @{{ registerForm.errors.get('invitation') }}
                </div>

                <!-- Registration Form -->
                @include('spark::auth.register-common-form')
            </div>
        </div>
    </div>
</div>
 --}}

 <div class="tile full-height-container auth secondary-overlay">
    <div class="container">
        <div class="object vertical-align">
            <div class="wrapper">

                <div class="object inner-container">

                    <div class="object intro">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <h1 class="object heading-style-one">Your Account Details</h1>
                    </div>

                    <form role="form">
                        {{ csrf_field() }}

                        @include('spark::shared.errors')

                        <!-- Account Type -->
                        <input type="hidden" v-model="registerForm.account_type" value="2" />

                        <ul>
                            <li>
                                <!-- Name -->
                                <div class="object input" :class="{'has-error': registerForm.errors.has('name')}">
                                    <label for="name" class="control-label">Your Name</label>
                                    <div class="input-wrapper">
                                        <input type="input" id="name" name="name" v-model="registerForm.name" placeholder="John Smith">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Company Name -->
                                <div class="object input" :class="{'has-error': registerForm.errors.has('company_name')}">
                                    <label for="name" class="control-label">Company Name</label>
                                    <div class="input-wrapper">
                                        <input type="input" id="company_name" name="company_name" v-model="registerForm.company_name" placeholder="The Wedding Company" >
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- Profile URL -->
                                <div class="object input" :class="{'has-error': registerForm.errors.has('profile_slug')}">
                                    <label for="name" class="control-label">Profile URL</label>
                                    <div class="input-wrapper">
                                        <input type="input" id="profile_slug" name="profile_slug" v-model="registerForm.profile_slug" v-on:change="formatSlug" placeholder="the-wedding-company" >
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- E-Mail Address -->
                                <div class="object input" :class="{'has-error': registerForm.errors.has('email')}">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <div class="input-wrapper">
                                        <input type="email" id="email" name="email" v-model="registerForm.email" placeholder="john.smith@budgetbride.co.uk">
                                    </div>
                                </div>
                            </li>
                            <li class="half" >
                                <!-- Password -->
                                <div class="object input" :class="{'has-error': registerForm.errors.has('password')}">
                                    <label for="password" class="control-label">Password</label>
                                    <div class="input-wrapper">
                                        <input type="password" id="password" name="password" v-model="registerForm.password" placeholder="••••••••">
                                    </div>
                                </div>
                            </li>
                            <li class="half">
                                <!-- Password Confirmation -->
                                <div class="object input" :class="{'has-error': registerForm.errors.has('password')}">
                                    <label for="password_confirmation" class="control-label">Confirm Password</label>
                                    <div class="input-wrapper">
                                        <input type="password" id="password_confirmation" name="password_confirmation" v-model="registerForm.password_confirmation" placeholder="••••••••">
                                    </div>
                                </div>
                            </li>
                            {{-- <li v-show="registerForm.errors.has('password')">
                                <div class="object help">
                                    @{{ registerForm.errors.get('password') }}
                                </div>
                            </li> --}}
                            <li class="terms" v-if=" ! selectedPlan || selectedPlan.price == 0">
                                <!-- Terms And Conditions -->
                                <div :class="{'has-error': registerForm.errors.has('terms')}">
                                    <div class="object checkbox">
                                        <label>
                                            <input type="checkbox" name="terms" v-model="registerForm.terms">
                                            I Accept The BudgetBrideUK Ltd <a href="/terms" target="_blank">Terms Of Service</a>
                                        </label>
                                        {{-- <span class="help-block" v-show="registerForm.errors.has('terms')">
                                            @{{ registerForm.errors.get('terms') }}
                                        </span> --}}
                                    </div>
                                </div>
                            </li>
                            <li class="submit">
                                <div class="object button-style-two black-primary">
                                    <button type="submit" @click.prevent="register" :disabled="registerForm.busy">
                                        <span v-if="registerForm.busy">
                                            <i class="fa fa-btn fa-spinner fa-spin"></i>Creating your profile
                                        </span>
                                        <span v-else>
                                            <i class="fa fa-btn fa-check-circle"></i>Start editing your profile
                                        </span>
                                    </button>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
