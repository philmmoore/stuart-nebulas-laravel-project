<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use \Laravel\Cashier\Cashier as Cashier;
use \Carbon\Carbon as Carbon;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'BudgetBrideUK Ltd',
        'product' => 'BudgetBride',
        'street' => '55 Devonshire Gardens',
        'location' => 'Southampton, SO31 8HE',
        // 'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = 'phil@budgetbride.co.uk';

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'phil@budgetbride.co.uk'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = false;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
 
        /**
         * Sets the default currency to gbp via cashier
         */

            Cashier::useCurrency('gbp', '£');

        /**
         * Include additional validation & storage for new account fields
         */
        
            Spark::validateUsersWith(function () {
                return [
                    'name' => 'required|max:255',
                    'account_type' => 'required|integer',
                    'company_name' => 'required',
                    'profile_slug' => 'required|unique:users',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
                    'vat_id' => 'max:50|vat_id',
                    'terms' => 'required|accepted',
                ];
            });

            Spark::createUsersWith(function ($request) {
                $user = Spark::user();

                $data = $request->all();

                $user->forceFill([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'company_name' => $data['company_name'],
                    'profile_slug' => $data['profile_slug'],
                    'account_type' => $data['account_type'],
                    'password' => bcrypt($data['password']),
                    'last_read_announcements_at' => Carbon::now(),
                    'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
                ])->save();

                return $user;
            });
            
        /**
         * Sets up app billing plans
         */
        
            Spark::useStripe()->noCardUpFront();

            // Spark::freePlan('Forever Free')
            //     ->features([
            //         'First', 'Second', 'Third'
            //     ]);

            Spark::plan('Standard', 'standard-2016')
                ->price(4.99)
                ->features([
                    'First', 'Second', 'Third'
                ]);

            Spark::plan('Standard', 'standard-2016')
                ->price(49.99)
                ->yearly()
                ->features([
                    'First', 'Second', 'Third'
                ]);

    }
}
