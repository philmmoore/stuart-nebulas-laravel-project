<?php

namespace App;

use App\Models\Location\County as County;
use Laravel\Spark\User as SparkUser;

class User extends SparkUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    /**
     * User models
     */
    public function meta(){
        return $this->hasMany('App\Models\User\Meta');
    }

    public function category(){
        return $this->hasMany('App\Models\User\Category');
    }

    public function location(){
        return $this->hasMany('App\Models\User\Location');
    }

    public function media(){
        return $this->hasMany('App\Models\User\Media');
    }

    /**
     * Get user meta data
     * @param  string  $key       
     * @param  boolean $as_object 
     */
    public function getMeta($key, $as_object=false){
        $meta = $this->meta()->select(['id', 'key', 'value'])->where('key', $key)->first();

        if (!$as_object && $meta){
            return $meta->value;
        } else if ($as_object == true) {
            return $meta;
        } else {
            return false;
        }
    }

    /**
     * Sets user meta data
     * @param string $key   
     * @param string $value 
     */
    public function setMeta($key, $value){

        // Trim the value to ensure there's no empty spaces when setting meta
        $value = trim($value);

        // First lets see if the meta key already exists, if it does then we'll want to update the existing model
        if ($meta = $this->getMeta($key, true)){
            // If the meta is an empty value lets remove the model
            if ($value == ''){
                $meta->delete();
            } else {
                $meta->value = $value;
                $meta->save();
            }
        }

        // Otherwise we'll need to create the Meta object
        else {
            // Only add the meta if the value is not null
            if ($value != ''){
                $meta = $this->meta()->create([
                    'key' => $key,
                    'value' => $value
                ]);
            }
        }

        return $meta;

    }

    /**
     * Gets the primary category
     */
    public function getPrimaryCategory(){

        $category = new \stdClass;
        $category->category_id = '';
        $category->prices_from = '';
        $category->prices_to = '';

        if ($primaryCategory = $this->category()->where('primary', 1)->first()){
            $category = $primaryCategory;
        }

        return $category;

    }

   /**
    * Sets the primary user category
    * @param integer $category_id       Category id
    * @param integer $prices_from       Price from
    * @param integer $prices_to         Price to
    */
   public function setPrimaryCategory($category_id, $prices_from = 0, $prices_to = 0){

        // Check if we already have a primary category set
        if ($category = $this->category()->where('primary', 1)->first()){
            $category->category_id = $category_id;
            $category->prices_from = $prices_from;
            $category->prices_to = $prices_to;
            $category->save();
        } else {
            $this->category()->create([
                'category_id' => $category_id,
                'primary' => 1,
                'prices_from' => $prices_from,
                'prices_to' => $prices_to
            ]);
        }

        return true;

   }


   /**
    * Gets all additional user categories
    * @return array     an array of additional category objects
    */
   public function getAdditionalCategories(){
        return $this->category()
                    ->leftJoin('categories', 'categories.id', '=', 'category_id')
                    ->where('primary', 0)
                    ->get();
   }

   /**
    * Sets additional user categories
    * @param array $categories      An array of key values associated with a category
    */
   public function setAdditionalCategories($categories){

        // Where we're mass assigning the categories it makes sense to remove all non
        // primary categories to then create a new entry, rather than performing a search for 
        // each category and updating it or creating one
        $this->category()->where('primary', 0)->delete();

        if ($categories){

            // Now we'll insert each new category 
            foreach ($categories as $category){
                $this->category()->create([
                    'category_id' => $category['category_id'],
                    'primary' => 0,
                    'prices_from' => $category['prices_from'],
                    'prices_to' => $category['prices_to']
                ]);
            }
        }

        return true;

   }
   
   /**
    * Checks if the user has a category of id
    * @param  int  $category_id     The category id that you would like to check exists
    * @return boolean              
    */
   public function hasCategory($category_id){
        $categories = $this->category()->pluck('category_id')->toarray();
        if (in_array($category_id, $categories)){
            return true;
        } else {
            return false;
        }
   }

   /**
    * Sets the primary location
    * @param int $county_id
    */
   public function setPrimaryLocation($county_id){

        // Check if we already have a primary location set
        if ($location = $this->location()->where('primary', 1)->first()){
            $location->county_id = $county_id;
            $location->save();
        } else {
            $this->location()->create([
                'county_id' => $county_id,
                'primary' => 1
            ]);
        }

        return true;

   }

   /**
    * Sets additional locations
    * @param array $locations       An array of county ids  
    */
   public function setAdditionalLocations($locations){

        // Where we're mass assigning the locations it makes sense to remove all non
        // primary locations to then create a new entry, rather than performing a search for 
        // each category and updating it or creating one
        $this->location()->where('primary', 0)->delete();

        if ($locations){

            // Now we'll insert each new category 
            foreach ($locations as $location){
                $this->location()->create([
                    'county_id' => $location,
                    'primary' => 0
                ]);
            }
        }

        return true;

   }

   /**
    * Checks if the user has a location of id
    * @param  int  $location_id     The location id that you would like to check exists
    * @return boolean              
    */
   public function hasLocation($location_id, $ignore_primary = false){
        if ($ignore_primary){
            $locations = $this->location()->where('primary', 0)->pluck('county_id')->toarray();
        } else {
            $locations = $this->location()->pluck('county_id')->toarray();
        }
        if (in_array($location_id, $locations)){
            return true;
        } else {
            return false;
        }
   }
   
   public function addMedia($original, $thumbnail){

        $media = $this->media()->create([
            'resource' => $original,
            'thumbnail' => $thumbnail,
            'position' => 0
        ]);

        return $media; 

   }

   public function getFullAddress(){

        $address = [];

        $address[] = $this->getMeta('addressLine1');
        $address[] = $this->getMeta('addressLine2');
        $address[] = $this->getMeta('addressTownCity');
        
        if ($county_id = $this->getMeta('addressCounty')){
            $address[] = County::where('id', $county_id)->first()->name;
        }

        $address[] = $this->getMeta('addressPostcode');

        return $address;

   }

   public function getMedia(){
        return $this->media()->get();
   }

}