<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function formatValidationErrors(Validator $validator)
    {	
    	/**
    	 * Spark expects errors to be returned in a specific format, i.e. errors.{field_key} => [message]
    	 * we're overriding laravels default format for Validator to keep things consistent
    	 */
    	$response['errors'] = [];
    	if ($validator->errors()->getMessages()){
    		foreach ($validator->errors()->getMessages() as $field => $value){
    			$response['errors'][$field] = $value;
    		}
    	}
        return $response;
    }

}
