<?php

namespace App\Http\Controllers\Spark\Settings\Supplier;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{

    /**
     * Update the user's primary category
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {

    	/**
    	 * Get the user from the request object
    	 */
    	$user = $request->user();

    	/**
    	 * Custom error messages for validator
    	 */

    	$messages = [];

    	/**
    	 * Validate form inputs
    	 */
		$this->validate($request, [
            'category' => 'required',
            'category_from' => 'required|numeric',
            'category_to' => 'sometimes|numeric'
		], $messages);

        /**
         * Set the new primary category
         */
        $user->setPrimaryCategory($request->category, $request->category_from, $request->category_to);

        /**
         * Set additional user categories if there are any present
         */
        if ($request->additional_categories){

            // Our fields are dynamic so we need to get the index of the items
            $validation_fields = [];
            $additional_validation_messages = [];
            
            // Validate each additional category
            foreach ($request->additional_categories as $index => $field){
                $validation_fields['additional_categories.'.$index.'.prices_from'] = 'required|numeric';
                $validation_fields['additional_categories.'.$index.'.prices_to'] = 'sometimes|numeric';
            }
            
            // Validate the fields
            $this->validate($request, $validation_fields, $additional_validation_messages);

        }

        // We always set the additional categories incase the user has decide to remove them all
        $user->setAdditionalCategories($request->additional_categories);

    }

    /**
     * Finds a single category for a given id 
     * @param  Request $Request     The request
     * @return JSON                 Returns the foud model in JSON format
     */
    public function get(Request $request){
        $category = new \App\Models\Categories\Category();
        return $category->find($request->id)->tojson();
    }

    public function getUserCategories(Request $request){
        return \Auth::user()->getAdditionalCategories()->tojson();
    }


}
