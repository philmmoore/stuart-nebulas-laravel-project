<?php

namespace App\Http\Controllers\Spark\Settings\Supplier;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{

    /**
     * The image manager instance.
     *
     * @var ImageManager
     */
    protected $images;

    /**
     * Create a new interaction instance.
     *
     * @param  ImageManager  $images
     * @return void
     */
    public function __construct(ImageManager $images)
    {
        $this->images = $images;
    }

    /**
     * Update the user's banner
     *
     * @param  Request  $request
     * @return String | Uploaded image URL
     */
    public function update(Request $request)
    {

        /**
         * Get the user from the request object
         */
        $user = $request->user();


        /**
         * Custom error messages for validator
         */

        $messages = [];

        /**
         * Validate input
         */
        $this->validate($request, [
            'banner' => 'required|image|max:4000'
        ], $messages);                    

        /**
         * Store the image
         */
        
        $file = $request->banner;
        $path = $file->hashName('banners/'.$user->id);

        // We will store the profile photos on the "public" disk, which is a convention
        // for where to place assets we want to be publicly accessible. Then, we can
        // grab the URL for the image to store with this user in the database row.
        $disk = Storage::disk('s3');

        $disk->put(
            $path, $this->formatImage($file)
        );

        // Url to disk image
        $imageURL = $disk->url($path);

        // Store the banner image in meta
        $user->setMeta('banner', $imageURL);

        // Return the image url for use with vue
        return $imageURL;

    }

    /**
     * Resize an image instance for the given file.
     *
     * @param  \SplFileInfo  $file
     * @return \Intervention\Image\Image
     */
    protected function formatImage($file)
    {
        return (string) $this->images->make($file->path())
                            ->fit(1440,600)
                            ->encode();
    }


}
