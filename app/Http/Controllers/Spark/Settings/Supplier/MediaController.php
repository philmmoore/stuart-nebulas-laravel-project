<?php

namespace App\Http\Controllers\Spark\Settings\Supplier;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{

    /**
     * The image manager instance.
     *
     * @var ImageManager
     */
    protected $images;

    /**
     * Create a new interaction instance.
     *
     * @param  ImageManager  $images
     * @return void
     */
    public function __construct(ImageManager $images)
    {
        $this->images = $images;
    }

    public function uploadFiles(Request $request) {
 
        /**
         * Get the user from the request object
         */
        $user = $request->user();

        /**
         * Custom error messages for validator
         */

        $messages = [];

        /**
         * Validate input
         */
        $this->validate($request, [
            'file' => 'required|image|max:10000'
        ], $messages);                    

        /**
         * Store the image
         */
        
        $file = $request->file;
        $path = 'media/'.$user->id;
        $name = md5_file($file);

        // Select storage disk
        $disk = Storage::disk('s3');

        // Put original image
        $file_original = $path.'/'.$name.'.'.$file->extension();
        $disk->put(
            $file_original,
            $this->formatImage($file)
        );

        // Thumbnail
        $file_thumbnail = $path.'/'.$name.'-thumbnail.'.$file->extension();
        $disk->put(
            $file_thumbnail,
            $this->formatAndResizeImage($file, 400, 400)
        );

        // Url to disk image
        $s3_original_url = $disk->url($file_original);
        $s3_thumb_url = $disk->url($file_thumbnail);

        // Store the media url in the database
        return $user->addMedia($s3_original_url, $s3_thumb_url);

    }

    /**
     * Resize an image instance for the given file.
     *
     * @param  \SplFileInfo  $file
     * @return \Intervention\Image\Image
     */
    protected function formatAndResizeImage($file, $width, $height)
    {
        return (string) $this->images->make($file->path())
                            ->fit($width, $height)
                            ->encode();
    }

    /**
     * Resize an image instance for the given file.
     *
     * @param  \SplFileInfo  $file
     * @return \Intervention\Image\Image
     */
    protected function formatImage($file)
    {
        return (string) $this->images->make($file->path())
                            ->encode();
    }

    public function getMedia(){
        return \Auth::user()->getMedia()->tojson();
    }

    public function deleteMedia(Request $request){
        $user = \Auth::user();
        $file = $user->media()->where('id', '=', $request->id)->get()->first();
        $file_original = pathinfo($file->resource);
        $file_thumb = pathinfo($file->thumbnail);
        $disk = Storage::disk('s3');
        $disk->delete('media/'.$user->id.'/'.$file_original['basename']);
        $disk->delete('media/'.$user->id.'/'.$file_thumb['basename']);
        $file->delete();
    }

}
