<?php

namespace App\Http\Controllers\Spark\Settings\Supplier;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{

    /**
     * Update the user's contact information settings.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {

    	/**
    	 * Get the user from the request object
    	 */
    	$user = $request->user();

    	/**
    	 * Custom error messages for validator
    	 */

    	$messages = [];


    	/**
    	 * Validate form inputs
    	 */
		$this->validate($request, [
            'company' => 'required',
            'profile_slug' => 'required|unique:users,profile_slug,'.$user->id
		], $messages);

		/**
		 * Set the field values passed from the request
		 */
        $user->setMeta('addressLine1', $request->addressLine1);
        $user->setMeta('addressLine2', $request->addressLine2);
        $user->setMeta('addressTownCity', $request->addressTownCity);
        $user->setMeta('addressPostcode', $request->addressPostcode);
        $user->setMeta('locationLatLng', $request->locationLatLng);
        $user->setMeta('website', $request->website);
        $user->setMeta('social_facebook', $request->social_facebook);
        $user->setMeta('social_twitter', $request->social_twitter);
        $user->setMeta('social_instagram', $request->social_instagram);
        $user->setMeta('telephone', $request->telephone);
        $user->setMeta('fax', $request->fax);
        $user->setMeta('mobile', $request->mobile);
        $user->setMeta('description', $request->description);

        /**
         * Update the company name
         */
        $user->company_name = $request->company;
        $user->profile_slug = $request->profile_slug;
        $user->save();

        /**
         * Set the users primary location
         */
        $user->setPrimaryLocation($request->addressCounty);

        /**
         * Set the users additional locations
         */
        $user->setAdditionalLocations($request->additionalLocations);

    }

}
