<?php

namespace App\Http\Controllers\Supplier;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User as User;

class ProfileController extends Controller
{
    
    // If a valid user the profile variable will be passed to the view
    public $profile;

 	/**
 	 * Before rendering a profile we first check that there is a valid profile
 	 * @param  Request $request 
 	 * @return view | 404          
 	 */
    public function show(Request $request){
    	if ($this->isRegisteredProfileSlug($request->profile_slug)){
	        return view('supplier/profile', [
                'profile' => $this->profile,
                'isLoggedInUser' => $this->isLoggedInUser($request->profile_slug)
            ]);
	    } else {
	    	abort(404);
	    }
    }

    /**
     * Checks the requested profile_slug against the currently logged in user to determing if 
     * they're viewing their own profile
     * @param  string  $profile_slug
     * @return boolean               
     */
    public function isLoggedInUser($profile_slug){
        if ($profile_slug === Auth::user()->profile_slug){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds a user with the given profile url or fails
     * @param  string  $slug
     * @return boolean       
     */
    public function isRegisteredProfileSlug($slug){

    	if ($this->profile = User::where('profile_slug', $slug)->firstOrFail()){
    		return true;
    	} else {
    		return false;
    	}

    }

}
