<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['resource', 'thumbnail', 'position'];

	 /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'user_id'];  

}
