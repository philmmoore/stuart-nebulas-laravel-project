<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'prices_from', 'prices_to', 'primary', 'created_at', 'updated_at'];

	 /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'user_id'];    

}
