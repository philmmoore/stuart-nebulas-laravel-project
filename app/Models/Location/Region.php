<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    /**
     * Get counties
     */
    public function counties(){
        return $this->hasMany('App\Models\Location\County');
    }

}
