<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    /**
     * Get regions
     */
    public function regions(){
        return $this->hasMany('App\Models\Location\Region');
    }

}
